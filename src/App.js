import React, { useReducer, useContext, useState } from "react";

// import './App.css';
import List from './components/list/List';
import { actions } from './components/actions/Actions';
import { TodoList } from "./index";
import { nanoid } from 'nanoid';

export default function App() {

  const Todos = useContext(TodoList);
  const [state, dispatch] = useReducer(itemReducer, Todos);
  const [input, setInput] = useState('');

  function add(title) {
    return {
      userId: 1,
      id: nanoid(),
      title: title,
      completed: false
    };
  };

  function itemReducer(state, { type, payload }) {

    switch (type) {
      case actions.ADD_ITEM:
        return [add(payload.value),
        ...state
        ];
      case actions.REMOVE_COMPLETED:
        return state.filter((item) => !item.completed ? item : null);
      case actions.REMOVE_ONE:
        return state.filter((item) => item.title !== payload.title)
      case actions.TOGGLE_COMPLETED:
        return state.map((item) => {
          if (item.title === payload.title) {
            return { ...payload, completed: !payload.completed };
          }
          return item
        })
      default:
        return;
    };
  };

  const handleNewItem = (event) => {
    if (event.keyCode === 13) {
      console.log(input)
      dispatch({ type: actions.ADD_ITEM, input, payload: { value: input } });
      setInput('')
    };
  };
  const handleCompleted = () => {
    dispatch({ type: actions.REMOVE_COMPLETED });
  };

  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos <br /> dumbie</h1>
        <input onChange={(e) => setInput(e.target.value)} onKeyDown={handleNewItem} value={input} className="new-todo" placeholder="What needs to be done, dumbie?" autoFocus />
      </header>
      <NewState.Provider value={state}>
        <List dispatch={dispatch} />
      </NewState.Provider>
      <footer className="footer">
        <span className="todo-count">
          <strong>{state.length}</strong> item(s) left
          </span>
        <button onClick={handleCompleted} className="clear-completed">Clear completed</button>
      </footer>
    </section>
  );
};
export const NewState = React.createContext();
