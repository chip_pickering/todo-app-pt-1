import React, { useState } from 'react';

import './Item.css';
import { actions } from '../actions/Actions';

export default function Item({ dispatch, title, completed }) {

    const [status] = useState(completed);
    const [itemClass] = useState(completed ? 'completed' : '');

    return (
        <li className={itemClass}>
            <div className="view">
                <input id={`${title}`} onChange={() => dispatch({ type: actions.TOGGLE_COMPLETED, payload: { title: title, completed: completed } })} className="toggle" type="checkbox" checked={status} />
                <label>{title + ' dumbie'}</label>
                <button onClick={() => dispatch({ type: actions.REMOVE_ONE, payload: { title: title } })} className="destroy" />
            </div>
        </li>
    );
};