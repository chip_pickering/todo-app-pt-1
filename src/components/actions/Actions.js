export const actions = {
    ADD_ITEM: 'add-item',
    REMOVE_COMPLETED: 'remove-completed',
    REMOVE_ONE: 'remove-one',
    TOGGLE_COMPLETED: 'toggle-completed',
};