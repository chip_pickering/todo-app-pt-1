import React, { useContext } from 'react';

import './List.css';
import { NewState } from '../../App';
import Item from '../item/Item';
import { nanoid } from 'nanoid';

export default function List({ dispatch }) {

    const Todos = useContext(NewState);

    return (
        <>
            <section className="main">
                <ul className="todo-list">
                    {Todos.map((todo) => (
                        <>
                            <Item key={nanoid()} title={todo.title} completed={todo.completed} dispatch={dispatch} />
                        </>
                    ))}
                </ul>
            </section>
        </>
    );
};